using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class TileBlock : AnimatedTile
{

    public string ObjectAnim;

    public void PlayAnimation()
    {
        GameObject gc = GameObject.FindGameObjectWithTag("GameController");

        Transform t = gc.transform.Find(ObjectAnim);
        
        if (t == null) return;

        Animation animation = t.GetComponent<Animation>();
        animation.Play();
    }

}
