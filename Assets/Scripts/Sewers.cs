using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sewers : MonoBehaviour
{
    [SerializeField]
    public Transform OutputSewer;

    [SerializeField]
    public int MoveYCamera;

    private Transform camera;
    private Transform player;
    private bool canEnter = false;

     

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player")
            .GetComponent<Transform>();
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) && canEnter == true )
        {
            camera.position = new Vector3(camera.position.x,  MoveYCamera, camera.position.z);
            player.position = OutputSewer.position;
        }
    }

     void OnTriggerEnter2D(Collider2D collision)
    {
       if(collision.gameObject.tag == "Player")
       {
            canEnter = true;
       }

    }

     void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            canEnter = false;
        }
       
    }
}
