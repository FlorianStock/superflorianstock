using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
   
    public Transform Target;

    public float Offset;

    public float SmoothTime = 0.3f;

    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        Offset = transform.position.x - Target.position.x;
    }

    private void LateUpdate()
    {
        if(Target != null)
        {
            Vector3 targetPosition = new Vector3(Target.position.x + Offset, transform.position.y, transform.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);
        }
    }
}
