using TreeEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Player : MonoBehaviour
{
    [SerializeField] private TileBase tileBloc;

    [SerializeField]
    private float Speed = 5;
    [SerializeField]
    private float JumpVelocity = 800;

    public Tilemap Tilemap;

    private Rigidbody2D body;
    private Animator animator;
    private bool isGrounded;
    private SpriteRenderer renderer;
    private TileAnimationData tileAnimationData;
    private object tileData;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        float move = Input.GetAxisRaw("Horizontal");
        body.velocity = new Vector2(Speed * move, body.velocityY);

        if(move != 0) renderer.flipX = body.velocity.x > 0;

              
        if ((Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Z)) && isGrounded == true)
        {
            body.AddForce(new Vector2(body.velocityX, JumpVelocity));
        }

        animator.SetBool("Run", move != 0);
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        isGrounded = true;
        animator.SetBool("IsGrounded", isGrounded);

        Grid grid = Tilemap.layoutGrid;
        foreach (ContactPoint2D contact in collision.contacts)
        {
            Vector3 contactPoint = contact.point - 0.05f * contact.normal;
            Vector3 gridPosition = grid.transform.InverseTransformPoint(contactPoint);
            Vector3Int cell = grid.LocalToCell(gridPosition);


            TileBase td = Tilemap.GetTile(cell);
            if (td != null)
            {
                if (td.GetType() == typeof(TileBlock))
                {
                    TileBlock block = (TileBlock)td;
                    block.PlayAnimation();
                    Tilemap.SetTile(cell, tileBloc);
                    Tilemap.RefreshTile(cell);
                }
            }        
        }
       
        
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        isGrounded = false;
        animator.SetBool("IsGrounded", isGrounded);
     
    }
}
