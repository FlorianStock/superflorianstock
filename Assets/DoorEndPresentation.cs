using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DoorEndPresentation : MonoBehaviour
{
    public GameObject ParticleSystemGO;
    private ParticleSystem ParticleSystem;

    // Start is called before the first frame update
    void Start()
    {
        ParticleSystem = ParticleSystemGO.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ParticleSystem.Play();
        Destroy(collision.gameObject);
    }
}
